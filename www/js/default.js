var app = {
	// Application Constructor
	initialize: function () {
		document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
	},

	// deviceready Event Handler
	//
	// Bind any cordova events here. Common events are:
	// 'pause', 'resume', etc.
	onDeviceReady: function () {
		this.receivedEvent('deviceready');
	},

	// Update DOM on a Received Event
	receivedEvent: function (id) {
		var permissions = cordova.plugins.permissions;
		permissions.hasPermission(permissions.INTERNET, function (status) {
			if (!status.hasPermission) {
				permissions.requestPermission(permissions.INTERNET, success, error);
			}
		});
	}

};

function error() {
	console.warn('Camera permission is not turned on');
}

function success(status) {
	if (!status.hasPermission) error();
}

app.initialize();

function returnHeaderHTML(userAvatar = "avatar.png", userNames = "John Doe", userEmail = "john.doe@gmail.com") {
	var navbar = '<nav class="teal accent-4"><div class="nav-wrapper"><a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a><a href="index.html" class="brand-logo"><img src="./img/logo-white.png" alt="Logo"></a><ul id="nav-mobile" class="right hide-on-med-and-down"><li><a href="./index.html">Events</a></li><li><a href="./groups.html">Groups</a></li><li><a href="./faq.html">FAQ</a></li></ul></div></nav>';
	var sidenav = '<ul id="slide-out" class="sidenav hide-on-med-and-up"><li><div class="user-view"><div class="background teal accent-4"></div><a href="#user"><img class="circle" src="img/defaults/' + userAvatar + '"></a><a href="#name"><span class="white-text name">' + userNames + '</span></a><a href="#email"><span class="white-text email">' + userEmail + '</span></a></div></li><li><a href="./index.html"><i class="material-icons">event</i>Upcoming Events</a></li><li><a href="./groups.html"><i class="material-icons">group_work</i>Groups</a></li><li><a href="./faq.html"><i class="material-icons">help</i>FAQ</a></li></ul>';
	return navbar + sidenav;
}

function includeHeader(bannerID = null) {
	var header = returnHeaderHTML();
	var body = $('body');
	body.prepend(header);
	var elem = document.querySelector('.sidenav');
	var instance = M.Sidenav.init(elem);
	if (bannerID != null) {
		var spacing = '';
		if (bannerID == 3) {
			spacing = 'top-spacing';
		}
		var banner = '<div class="banner ' + spacing + '"><img src="./img/banners/' + bannerID + '.jpg" alt="Banner"></div>';
		body.prepend(banner);
	}
}

function initAutocomplete() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: 42.894565,
			lng: 25.303713
		},
		zoom: 6,
		mapTypeId: 'roadmap'
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('location');
	var searchBox = new google.maps.places.SearchBox(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function () {
		searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function () {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		markers.forEach(function (marker) {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function (place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);
	});
}

function initMap() {
	var uluru = {
		lat: 42.894565,
		lng: 25.303713
	};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 13,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}